# README

This quick and dirty script converts the RSS feed generated from Fargo.io to Jekyll-compatible text files. It also appends a description with a no-follow permalink to the original post.

Fargo looked like an interesting project, but Dave had trouble scaling his JavaScript web server and our material was inaccessible for some time. He's since moved to Heroku, but I've decided I'd rather host my own material now.

## Using

    $ cd ~/Dropbox/Apps/Fargo/rss/
    $ ./fargo-to-jekyll.pl <your-outline.xml>
    $ cp *html </your-jekyll-site/_posts/>
    
Fargo doesn't have any category/tag taxonomy, so script assigns to "internet" category and tags it as "imported-from-fargo". You can customise these with the `$category` and `$tag` scalars.

## Dependencies

* [Time::Piece](https://metacpan.org/pod/Time::Piece) by [Ricardo Signes](https://metacpan.org/author/RJBS). Part of Perl since 5.08
* [RSS::XML](https://metacpan.org/pod/RSS::XML) by [Schlomi Fish](https://metacpan.org/author/SHLOMIF).

## Links
* [Fargo.io](http://fargo.io/), Dave Winer's web based outliner.
* [Jekyll](http://jekyllrb.com/), simple, blog-aware, static sites.

## Bugs and caveats

* Likely many. 

* Assumes all dates are GMT, because that's what Fargo uses. `Time::Piece` also had trouble with date masks on my Mavericks Mac, so I used methods instead.

* Is also very verbose by Perl standards, because I suck at golf and prefer readability.

* I'm unsure how many posts Fargo exports to RSS. For my Fargo outline, it was sufficient. If you have more, I suppose the script would need to be rewritten to parse the original OPML, or you'd have to import in batches.
