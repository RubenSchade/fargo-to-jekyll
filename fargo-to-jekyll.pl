#!/usr/bin/env perl -w

###########################################################################
##                                                                       ##
##  A quick Fargo to Jekyll converter using XML::RSS                     ##
##  Takes RSS feed from your /<dropbox>/Apps/Fargo/ as parameter, then   ##
##  generates Jekyll (and Octopress, et al) compatible text files        ##
##                                                                       ##
##  Fargo was a cute, web based, Dropbox-backed XML outliner by Dave     ##
##  Winer that published to smallpict.com. Looked cool, but his          ##
##  webserver broke. Host your own stuff, people!                        ##
##                                                                       ##
##  By Ruben Schade 2014                                                 ##
##  Released under the MIT licence/license (poe-tay-toe, poe-tahr-toe)   ##
##                                                                       ##
###########################################################################

## INCLUDES
use 5.010;
use strict;
use warnings( FATAL => 'all' );  ## The Marco Arment Principle
use utf8;
use open ':encoding(utf8)';  ## let's avoid "Wide character in print" errors!

## DEPENDENCIES
use Time::Piece;  ## Core since Perl 5.9
use XML::RSS;     ## May need installing from cpan, depending on distro

## CONSTANTS
use constant { SUCCESS => 0 };

## TAXONOMY
my $category = 'internet';
my $tag = 'imported-from-fargo';

######
##  I heard you like scope limiting main methods in scripting languages
##
sub main {
    my $filename = shift;
    my $site = "";
    
    ## Populate RSS parser with our provided file
    my $rss = XML::RSS->new;
    $rss->parsefile($filename) || die "Can't parse!";
    
    ## Get master site URL
    $site = $rss->{'channel'}->{'link'};
    
    ## Generate new post from each RSS item
    foreach my $item (@{$rss->{'items'}}) {
        say("Processing post: ". $item->{'title'});
        
        &new_jekyll_post(
            $item->{'title'},
            $item->{'pubDate'},
            $item->{'link'},
            $item->{'description'},
            $site
        );
    }
    
    return SUCCESS;
}

######
##  Generates new Jekyll textfiles
##
sub new_jekyll_post {
    my ($title, $date, $url, $desc, $site) = @_;
    
    ## Open new filename based on date and permalink
    my $filename = &file_name($date, $url);
    open(my $handle, '>', $filename);
    
    ## Print out Jekyll formatted post to file
    print $handle ("---\n".
        "layout: post\n".
        "title: \"$title\"\n".
        "date: ". &parse_rss_date($date). "\n".
        "category: $category\n".
        "tags:\n".
        "- $tag\n".
        "---".
        &clean($desc).
        &footer($title, $url, $site)
    );
    
    close($handle);
}

######
##  Removes errant tabs, and converts Fargo HTML to Modern HTML. Yes, it's
##  bad regex and I should feel bad.
##
sub clean {
    my $line = shift;
    
    $line =~ s/\t//g;
    $line =~ s/<i>/<em>/g;
    $line =~ s/<\/i>/<\/em>/g;
    $line =~ s/b>/strong>/g;
    
    return $line;
}

######
##  Converts RSS to date Jekyll can digest. I've had more success with
##  spaces, rather than ISO mandated T and no space between time and tz.
sub parse_rss_date {
    my $rss_date = shift;
    
    my $iso_date = Time::Piece->strptime($rss_date);
    return $iso_date->ymd. ' '. $iso_date->time. ' +0000';
}

######
##  Create Jekyll filename from publish date and original permalink
sub file_name {
    my ($date, $link) = @_;
    
    ## Convert date to year-month-date
    my $ymd = Time::Piece->strptime($date);
    
    ## Pop last part of Fargo URL, to use for local permalink
    my @permalink = split(m/\//, $link);
    my $title = pop(@permalink);
    undef @permalink;
    
    return $ymd->ymd. '-'. $title. '.html';    
}

######
##  Generate footer to append to imported posts, with original URL
##
sub footer {
    my ($title, $url, $site) = @_;
    
    ## I use the site for the link, so making it pretty
    $site =~ s/^http:\/\///;
    $site =~ s/\/$//;
    
    return '<p style="font-style:italic">This post originally appeared '.
           "on <a rel=\"nofollow\" title=\"$title\" href=\"$url\"". 
           ">$site</a>.</p>\n";
}

######
##  Parse parameters the quick and dirty way (hey, Perl!)
##
if (@ARGV != 1) {
    die("Usage: fargo-to-jekyll.pl <fargoRSSFile.xml>\n");
}
else {
    exit(&main($ARGV[0]));
}

__END__
